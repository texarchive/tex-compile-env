FROM archlinux
RUN pacman -Sy \
    && pacman -S --noconfirm \
       texinfo \
       texlive-most \
       zip \
       unzip \
       biber
